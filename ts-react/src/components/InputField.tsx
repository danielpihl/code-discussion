import React from 'react';

interface Props {
    value: string;
    label: string;
    name: string;
    onChange: (value: string) => void
}

export default function InputField({ value, label, name, onChange }: Props) {
    return (
        <div className="form-group">
            <label htmlFor={ name }>{ label } </label>
            <input
                className="form-control"
                type="text"
                name={ name }
                value={ value }
                onChange={ (event) => onChange(event.target.value) }
            />
        </div>
    )
}
