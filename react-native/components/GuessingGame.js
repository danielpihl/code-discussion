import React, {Component} from 'react';
import {Button, ScrollView, StyleSheet, Text, View} from 'react-native';
import InputField from './InputField';
import Leaderboard from './Leaderboard';

export default class GuessingGame extends Component {

    constructor(props) {
        super(props);

        this.state = {
            answer: Math.floor(Math.random() * 100),
            guesses: 0,
            hint: '',
            name: '',
            scores: [{name: 'SK', score: 1337}, {name: 'Love', score: 666}],
            started: false,
        };
    }

    onChangeName = (name) => {
        this.setState({name});
    };

    startGame = () => {
        this.setState({started: true});
    };

    makeGuess = () => {
        this.setState({guesses: this.state.guesses + 1});

        if (parseInt(this.props.guess) === this.state.answer) {
            this.setState({
                answer: Math.floor(Math.random() * 100),
                guesses: 0,
                hint: '',
                scores: this.state.scores.concat({name: this.state.name, score: this.state.guesses + 1}),
            });
        } else if (parseInt(this.props.guess) > this.state.answer) {
            this.setState({hint: 'Try lower!'});
        } else {
            this.setState({hint: 'Try higher!'});
        }

        this.props.setGuess('');
    };

    render() {
        return (
            <ScrollView>
                <View style={styles.header}>
                    <Text style={styles.title}>Guessing Game</Text>
                    <Text style={styles.description}>Guess a number between 0-{this.props.difficulty}</Text>
                </View>

                <View style={styles.container}>
                    {!this.state.started && (
                        <View>
                            <InputField label={'Enter your name:'} value={this.state.name}
                                        onChange={this.onChangeName}/>
                            <Button title={'Start!'} onPress={this.startGame}/>
                        </View>
                    )}

                    {this.state.started && (
                        <View>
                            <Text>Total guesses: {this.state.guesses}</Text>
                            {this.state.hint !== undefined && <Text>{this.state.hint}</Text>}
                            <InputField label={'Guess:'} value={this.props.guess}
                                        onChange={this.props.setGuess}/>
                            <Button title={'Guess'} onPress={this.makeGuess}/>
                        </View>
                    )}

                    <Leaderboard scores={this.state.scores}/>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: 'lightgray',
        marginBottom: 12,
        paddingVertical: 24,
    },
    title: {
        fontSize: 32,
    },
    description: {
        fontSize: 18,
    },
    container: {
        margin: 24,
    },
});
