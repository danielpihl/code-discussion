import React from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';

export default function InputField({ value, label, onChange }) {
    return (
        <View>
            <Text>{ label } </Text>
            <TextInput
                value={ value }
                style={styles.textInput}
                onChangeText={ onChange }
            />
        </View>
    )
}

const styles = StyleSheet.create({
    textInput: {
        backgroundColor: '#f2f2f2',
        paddingHorizontal: 12,
        marginVertical: 12,
    },
})
